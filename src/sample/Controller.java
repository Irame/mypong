package sample;

import javafx.animation.*;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Controller {
    @FXML Circle Circle1;
    @FXML Rectangle Rectangle1;

    public void firstButtonOnAction(ActionEvent actionEvent) {
        double moveH = 100;
        double moveV = 100;

        SequentialTransition sequentialTransition = new SequentialTransition();
        sequentialTransition.getChildren().addAll(
                createTranslateTransition(moveH,0,300),
                createTranslateTransition(moveH,moveV,300),
                createTranslateTransition(0,moveV,300),
                createTranslateTransition(0,0,300)
        );
        sequentialTransition.setCycleCount(1);

        RotateTransition rotateTransition = new RotateTransition(new Duration(1200), Rectangle1);
        rotateTransition.setFromAngle(0);
        rotateTransition.setToAngle(4*360);

        FillTransition fillTransition = new FillTransition();
        fillTransition.setDuration(new Duration(1200));
        fillTransition.setShape(Rectangle1);
        fillTransition.setFromValue(Color.BLACK);
        fillTransition.setToValue(Color.RED);

        FadeTransition fadeTransition = new FadeTransition(new Duration(600), Rectangle1);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0.1);

        FadeTransition fadeTransitionRev = new FadeTransition(new Duration(600), Rectangle1);
        fadeTransitionRev.setFromValue(0.1);
        fadeTransitionRev.setToValue(1);



        ParallelTransition parallelTransition = new ParallelTransition(
                sequentialTransition,
                rotateTransition,
                fillTransition,
                (new SequentialTransition(fadeTransition, fadeTransitionRev))
        );


        parallelTransition.play();
    }

    private TranslateTransition createTranslateTransition(double moveH, double moveV, double duration){
        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setDuration(new Duration(duration));
        translateTransition.setToX(moveH);
        translateTransition.setToY(moveV);
        translateTransition.setNode(Rectangle1);

        return translateTransition;
    }
}
