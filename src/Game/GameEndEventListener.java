package Game;

/**
 * Created by Felix on 27.03.2014.
 */
@FunctionalInterface
public interface GameEndEventListener {
    public void onGameEnd(GameEndEvent gameEndEvent);
}
