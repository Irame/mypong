package Game;

import GUI.GUI;
import GUI.Controller;
import javafx.animation.TranslateTransition;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * Created by Felix on 24.03.2014.
 */
public class Player {
    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_DOWN = -1;

    private int playerNumber;
    private boolean barMoveUp = false;
    private boolean barMoveDown = false;
    private int score = 0;
    private String name;
    private KeyCode upKey;
    private KeyCode downKey;
    private Rectangle playerBar;
    private TranslateTransition playerBarTT;
    private double barSpeed = 0.85;        //Milliseconds per Pixel

    public Player(int number, String name) {
        this.playerNumber = number;
        this.name = name;
        if (number == 1) {
            upKey = KeyCode.W;
            downKey = KeyCode.S;
        } else if (number == 2) {
            upKey = KeyCode.UP;
            downKey = KeyCode.DOWN;
        }
    }

    public void moveBar(GUI gui, int direction) {
        setBarDirection(direction);
        TranslateTransition playerBarTT = getPlayerBarTT();
        if (direction < 0) {
            playerBarTT.setDuration(getTimeToBottom(gui));
            playerBarTT.setToY(getBottomBound(gui));
        } else if (direction > 0) {
            playerBarTT.setDuration(getTimeToTop());
            playerBarTT.setToY(getTopBound());
        }
        playerBarTT.play();
    }

    public void stopBar() {
        setBarDirection(0);
        getPlayerBarTT().stop();
    }

    public void moveBar(GUI gui, int direction, double dist) {
        setBarDirection(direction);
        Rectangle playerBar = getPlayerBar();
        double curYPos = playerBar.getTranslateY();
        TranslateTransition playerBarTT = getPlayerBarTT();
        if (direction < 0) {
            double bottomBound = getBottomBound(gui);
            double destY = curYPos + dist >= bottomBound ? bottomBound : curYPos + dist;
            playerBarTT.setDuration(new Duration(dist * barSpeed));
            playerBarTT.setToY(destY);
        } else if (direction > 0) {
            double topBound = getTopBound();
            double destY = curYPos + dist <= topBound ? topBound : curYPos - dist;
            playerBarTT.setDuration(new Duration(dist * barSpeed));
            playerBarTT.setToY(destY);
        }
        playerBarTT.play();
    }

    private double getTopBound() {
        return getPlayerBar().getY();
    }


    private double getBottomBound(GUI gui) {
        double initYPos = getPlayerBar().getY();
        double height = getPlayerBar().getHeight();
        double sceneHeight = gui.getSceneHeight();

        return sceneHeight - (height + 2 * initYPos);
    }

    private Duration getTimeToTop() {
        return new Duration(getPlayerBar().getTranslateY() * barSpeed);
    }

    private Duration getTimeToBottom(GUI gui) {
        double initYPos = getPlayerBar().getY();
        double curYPos = getPlayerBar().getTranslateY();
        double height = getPlayerBar().getHeight();
        double sceneHeight = gui.getSceneHeight();

        return new Duration((sceneHeight - height + 2 * initYPos - curYPos) * barSpeed);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setBarDirection(int direction){
        if (direction >= DIRECTION_UP) {
            setBarMoveUp(true);
            setBarMoveDown(false);
        } else if (direction <= DIRECTION_DOWN) {
            setBarMoveUp(false);
            setBarMoveDown(true);
        } else {
            setBarMoveUp(false);
            setBarMoveDown(false);
        }
    }

    public int getBarDirection() {
        return (barMoveUp ? DIRECTION_UP : 0) + (barMoveDown ? DIRECTION_DOWN : 0);
    }

    public void setBarMoveUp(boolean set) {
        barMoveUp = set;
    }

    public void setBarMoveDown(boolean set) {
        barMoveDown = set;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public void setUpKey(KeyCode upKey) {
        this.upKey = upKey;
    }

    public KeyCode getDownKey() {
        return downKey;
    }

    public void setDownKey(KeyCode downKey) {
        this.downKey = downKey;
    }

    public Rectangle getPlayerBar() {
        return playerBar;
    }

    public void setPlayerBar(Rectangle playerBar) {
        this.playerBar = playerBar;
    }

    public TranslateTransition getPlayerBarTT() {
        return playerBarTT;
    }

    public void setPlayerBarTT(TranslateTransition playerBarTT) {
        this.playerBarTT = playerBarTT;
    }

    public double getBarSpeed() {
        return barSpeed;
    }

    public void setBarSpeed(double barSpeed) {
        this.barSpeed = barSpeed;
    }
}