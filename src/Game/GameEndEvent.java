package Game;

/**
 * Created by Felix on 27.03.2014.
 */
public class GameEndEvent {
    private Player winner;
    private Player loser;
    private int bounceCount;

    public GameEndEvent(Player winner, Player loser, int bounceCount){
        this.winner = winner;
        this.loser = loser;
        this.bounceCount = bounceCount;
    }

    public int getWinnerNumber(){
        return winner.getPlayerNumber();
    }

    public int getLoserNumber(){
        return loser.getPlayerNumber();
    }

    public Player getPlayer(int number){
        if (number == winner.getPlayerNumber()) return winner;
        if (number == loser.getPlayerNumber()) return loser;
        return null;
    }
    public int getBounceCount(){
        return bounceCount;
    }
}
