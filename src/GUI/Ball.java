package GUI;

import javafx.animation.TranslateTransition;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * Created by Felix on 24.03.2014.
 */
public class Ball {
    private double direction = 0;
    private double pixelPerMilli = 0;
    private Circle ballCircle;
    private TranslateTransition ballTT;

    public Ball(Circle ballCircle) {
        this.ballCircle = ballCircle;
    }

    public void move(GUI gui) {
        TranslateTransition ballTT = getBallTT();
        Circle ballCircle = getBallCircle();
        ballTT.stop();

        double curX = getX(), curY = getY();
        double sceneHeight = gui.getSceneHeight(), sceneWidth = gui.getSceneWidth();
        double direction = getDirection();
        double cosDir = Math.cos(direction), sinDir = Math.sin(direction), tanDir = Math.tan(direction);

        double destX, destY;

        if (cosDir > 0) destY = curY + (sceneWidth - curX) * tanDir;
        else            destY = curY + (0 - curX) * tanDir;

        if (sinDir > 0) destX = (sceneHeight - curY + curX * tanDir) / tanDir;
        else            destX = (0 - curY + curX * tanDir) / tanDir;

        if (destY < 0)                  destY = 0;
        else if (destY > sceneHeight)   destY = sceneHeight;
        else if (destX < 0)             destX = 0;
        else if (destX > sceneWidth)    destX = sceneWidth;


        System.out.println(String.format("Dest: x = %f | y = %f", destX, destY));

        ballTT.setDuration(new Duration(Math.sqrt(Math.pow(destX - curX, 2) + Math.pow(destY - curY, 2)) / getPixelPerMilli()));
        destX = destX - ballCircle.getCenterX();
        destY = destY - ballCircle.getCenterY();
        ballTT.setToX(destX);
        ballTT.setToY(destY);
        ballTT.play();
    }

    public void setRelPosition(double xPos, double yPos) {
        ballCircle.setTranslateX(xPos);
        ballCircle.setTranslateY(yPos);
    }

    public double getX() {
        return ballCircle.getCenterX() + ballCircle.getTranslateX();
    }

    public double getY() {
        return ballCircle.getCenterY() + ballCircle.getTranslateY();
    }

    public double getDirection() {
        return direction;
    }

    public void setDirection(double direction) {
        this.direction = direction;
    }

    public double getPixelPerMilli() {
        return pixelPerMilli;
    }

    public void setPixelPerMilli(double pixelPerMilli) {
        this.pixelPerMilli = pixelPerMilli;
    }

    public TranslateTransition getBallTT() {
        return ballTT;
    }

    public void setBallTT(TranslateTransition ballTT) {
        this.ballTT = ballTT;
    }

    public Circle getBallCircle() {
        return ballCircle;
    }

    public double getRate() {
        return ballTT.getRate();
    }

    public String toString() {
        return String.format("[ x="+getX()+", y="+getY()+", dir="+getDirection()+", speed="+getPixelPerMilli()+", rate="+getRate());
    }

    public double getRelX() {
        return ballCircle.getTranslateX();
    }

    public double getRelY() {
        return ballCircle.getTranslateY();
    }
}
