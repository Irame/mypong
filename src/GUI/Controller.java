package GUI;

import Game.GameEndEvent;
import Game.Player;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;

/**
 * Created by Felix on 24.03.2014.
 */
public class Controller {
    @FXML private Text scoreLabel;
    @FXML private Button startButton;
    @FXML private Pane gameField;
    @FXML private Rectangle player1Bar;
    @FXML private Rectangle player2Bar;
    @FXML private Circle ballCircle;

    private GUI gui;


    public Ball init(GUI gui) {
        this.gui = gui;
        Player player1 = gui.getPlayer1();
        Player player2 = gui.getPlayer2();

        TranslateTransition player1BarTT;
        TranslateTransition player2BarTT;
        TranslateTransition ballTT;

        player1BarTT = new TranslateTransition();
        player1BarTT.setNode(player1Bar);
        player1BarTT.setInterpolator(Interpolator.LINEAR);
        player1.setPlayerBar(player1Bar);
        player1.setPlayerBarTT(player1BarTT);

        player2BarTT = new TranslateTransition();
        player2BarTT.setNode(player2Bar);
        player2BarTT.setInterpolator(Interpolator.LINEAR);
        player2.setPlayerBar(player2Bar);
        player2.setPlayerBarTT(player2BarTT);

        Ball ball = new Ball(ballCircle);
        ballTT = new TranslateTransition();
        ballTT.setNode(ballCircle);
        ballTT.setInterpolator(Interpolator.LINEAR);
        ball.setBallTT(ballTT);

        player2Bar.xProperty().bind(getSceneWidthProperty().subtract(20));

        player1Bar.heightProperty().bind(getSceneHeightProperty().subtract(player1Bar.getY() * 2).divide(4));
        player2Bar.heightProperty().bind(getSceneHeightProperty().subtract(player2Bar.getY() * 2).divide(4));

        getSceneHeightProperty().addListener((observableValue, number, number2) -> {
            player1Bar.setTranslateY(player1Bar.getTranslateY() * ((number2.doubleValue() - player1Bar.getY() * 2) / (number.doubleValue() - player1Bar.getY() * 2)));
            player2Bar.setTranslateY(player2Bar.getTranslateY() * ((number2.doubleValue() - player2Bar.getY() * 2) / (number.doubleValue() - player2Bar.getY() * 2)));
        });

        ballCircle.centerXProperty().bind(getSceneWidthProperty().divide(2));
        ballCircle.centerYProperty().bind(getSceneHeightProperty().divide(2));


        return ball;
    }

    public double getSceneHeight() {
        return gameField.getHeight();
    }

    public double getSceneWidth() {
        return gameField.getWidth();
    }

    public ReadOnlyDoubleProperty getSceneHeightProperty() {
        return gameField.heightProperty();
    }

    public ReadOnlyDoubleProperty getSceneWidthProperty() {
        return gameField.widthProperty();
    }

    public void SetScore(GameEndEvent gameEndEvent){
        Player winner = gameEndEvent.getPlayer(gameEndEvent.getWinnerNumber());
        Player loser = gameEndEvent.getPlayer(gameEndEvent.getLoserNumber());
        winner.setScore(winner.getScore()+1);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                scoreLabel.setText(String.format("Score: %d:%d",
                        winner.getPlayerNumber() == 1 ? winner.getScore() : loser.getScore(),
                        winner.getPlayerNumber() == 2 ? winner.getScore() : loser.getScore()));
            }
        });

    }

    public void startButtonClicked(ActionEvent actionEvent) {
        gui.startGame();
    }
}