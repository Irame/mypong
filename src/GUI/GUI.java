package GUI;

import Game.GameEndEvent;
import Game.Player;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class GUI extends Application {
    private static Player player1Static;
    private static Player player2Static;
    private static GUI curGUI;

    private Player player1;
    private Player player2;
    private Ball ball;
    private GameWorker gameWorker;
    private Controller controller;

    public GUI() {
        System.out.println("Load GUI");
        this.player1 = player1Static;
        this.player2 = player2Static;
        curGUI = this;
    }

    public static GUI create(Player player1, Player player2) {
        player1Static = player1;
        player2Static = player2;
        launch();
        return curGUI;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GUI.fxml"));
        primaryStage.setTitle("MyPong");
        Scene scene = new Scene(fxmlLoader.load(), 300, 275);
        controller = fxmlLoader.getController();
        ball = controller.init(this);


        scene.setOnKeyPressed((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == player1.getUpKey())
                player1.moveBar(this, Player.DIRECTION_UP);
            else if (keyEvent.getCode() == player1.getDownKey())
                player1.moveBar(this, Player.DIRECTION_DOWN);
            else if (keyEvent.getCode() == player2.getUpKey())
                player2.moveBar(this, Player.DIRECTION_UP);
            else if (keyEvent.getCode() == player2.getDownKey())
                player2.moveBar(this, Player.DIRECTION_DOWN);
        });
        scene.setOnKeyReleased((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == player1.getUpKey() || keyEvent.getCode() == player1.getDownKey())
                player1.stopBar();
            else if (keyEvent.getCode() == player2.getUpKey() || keyEvent.getCode() == player2.getDownKey())
                player2.stopBar();
        });

        primaryStage.setMinWidth(200);
        primaryStage.setMinHeight(150);
        primaryStage.setScene(scene);
        primaryStage.show();

        player1.moveBar(this, Player.DIRECTION_DOWN, (controller.getSceneHeight()-player1.getPlayerBar().getHeight())/2);
        player2.moveBar(this, Player.DIRECTION_DOWN, (controller.getSceneHeight()-player2.getPlayerBar().getHeight())/2);

        gameWorker = new GameWorker(this);
        Thread thread = new Thread(gameWorker);
        thread.start();

        primaryStage.setOnCloseRequest((WindowEvent windowEvent) -> {
            if (gameWorker != null) {
                gameWorker.stop();
            }
        });
    }

    public Controller getController() {
        return controller;
    }

    public double getSceneWidth(){
        return controller.getSceneWidth();
    }

    public double getSceneHeight() {
        return controller.getSceneHeight();
    }

    public Ball getBall() {
        return ball;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void startGame() {
        ball.setDirection((Math.random() * Math.PI/4-Math.PI/8) + Math.PI*Math.floor(Math.random()*2));
        ball.setPixelPerMilli(0.3);
        ball.setRelPosition(0, 0);
        ball.move(this);
        gameWorker.reset();
        gameWorker.enableBallMovement();
        gameWorker.addGameEndEventListener((gameEndEvent) -> {
            System.out.printf("Winner: %s!%nLoser: %s!%nBouncecount: %d%n",
                    gameEndEvent.getPlayer(gameEndEvent.getWinnerNumber()).getName(),
                    gameEndEvent.getPlayer(gameEndEvent.getLoserNumber()).getName(),
                    gameEndEvent.getBounceCount());
            controller.SetScore(gameEndEvent);
        });
    }
}