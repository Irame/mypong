package GUI;

import Game.GameEndEvent;
import Game.GameEndEventListener;
import Game.Player;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

/**
 * Created by Felix on 24.03.2014.
 */
public class GameWorker implements Runnable {
    private GUI gui;
    private volatile boolean isRunning;
    private boolean updateBallMovement;
    private double lastRelXPos = 0;
    private boolean rightSideHit = false;
    private boolean leftSideHit = false;
    private int loopSleep;
    private int bounceCount = 0;
    private ArrayList<GameEndEventListener> gameEndEventListenerList;

    public GameWorker(GUI gui) {
        this(gui, 5);
    }

    public GameWorker(GUI gui, int loopSleepMillis){
        this.gui = gui;
        updateBallMovement = false;
        loopSleep = loopSleepMillis;
        gameEndEventListenerList = new ArrayList<>();

        /*
        gui.getBall().getBallCircle().translateXProperty().addListener((observableValue, number, number2) -> {
            ballMovement(0);
        });
        */
    }

    @Override
    public void run() {
        isRunning = true;
        long lastExecute = 0;

        while (isRunning) {
            long curTime = System.nanoTime();
            long elapsed = curTime - lastExecute;
            lastExecute = curTime;

            if (updateBallMovement)
                ballMovement(elapsed);

            easySleep(loopSleep);
        }
    }

    private void ballMovement(long elapsed) {
        Player player1 = gui.getPlayer1();
        Player player2 = gui.getPlayer2();
        Ball ball = gui.getBall();

        Rectangle player1Bar = player1.getPlayerBar();
        Rectangle player2Bar = player2.getPlayerBar();
        double p1BarTopBound = player1Bar.getTranslateY() + player1Bar.getY();
        double p2BarTopBound = player2Bar.getTranslateY() + player2Bar.getY();
        double p1BarBottomBound = p1BarTopBound + player1Bar.getHeight();
        double p2BarBottomBound = p2BarTopBound + player2Bar.getHeight();


        double yPos = ball.getY();
        double xPos = ball.getX();
        double yRelPos = ball.getRelY();
        double xRelPos = ball.getRelX();
        double sceneHeight = gui.getSceneHeight();
        double sceneWidth = gui.getSceneWidth();
        double direction = ball.getDirection();
        double sinDirection = Math.sin(direction);

        if (lastRelXPos == xRelPos && (xPos == 0 || xPos == gui.getSceneWidth())) {
            if (ball.getRelX() < 0) {
                disableBallMovement();
                fireGameEndEvent(new GameEndEvent(player2, player1, bounceCount));
                return;
            } else if (ball.getRelX() > 0) {
                disableBallMovement();
                fireGameEndEvent(new GameEndEvent(player1, player2, bounceCount));
                return;
            }
        }



        if (leftSideHit && xRelPos > 0) leftSideHit = false;
        if (rightSideHit && xRelPos < 0) rightSideHit = false;

        if ((yPos < 15 && sinDirection < 0) ||                       //Top bound Hit
            (yPos > sceneHeight - 15 && sinDirection > 0)) {         //Bottom bound Hit
            ball.setDirection(-direction);
            ball.move(gui);
        } else if ((xPos <= 30 && lastRelXPos != xRelPos && !leftSideHit && yPos >= p1BarTopBound-5 && yPos <= p1BarBottomBound+5)) {      //Left bar Hit
            leftSideHit = true;
            ball.setDirection((yPos-p1BarTopBound)/player1.getPlayerBar().getHeight()*Math.PI/2 - Math.PI*1/4);
            ball.setPixelPerMilli(ball.getPixelPerMilli() * 1.1);
            ball.move(gui);
            bounceCount++;
        } else if ((xPos >= sceneWidth - 30 && lastRelXPos != xRelPos && !rightSideHit && yPos >= p2BarTopBound-5 && yPos <= p2BarBottomBound+5)) {     //Right bar Hit
            rightSideHit = true;
            ball.setDirection((yPos-p2BarTopBound)/player2.getPlayerBar().getHeight()*-Math.PI/2 - Math.PI*3/4);
            ball.setPixelPerMilli(ball.getPixelPerMilli() * 1.1);
            ball.move(gui);
            bounceCount++;
        } else if (xPos < 25 && xPos > 5 && yPos < p1BarTopBound && yPos > p1BarTopBound - 10 && sinDirection>0 && !leftSideHit ||                                     //Left bar top bound Hit
                xPos < 25 && xPos > 5 && yPos > p1BarBottomBound && yPos < p1BarBottomBound + 10 && sinDirection<0 && !leftSideHit ||                               //Left bar bottom bound Hit
                xPos > sceneWidth - 25 && xPos < sceneWidth - 5 && yPos < p2BarTopBound && yPos > p2BarTopBound - 10 && sinDirection>0 && !rightSideHit ||           //Right bar top bound Hit
                xPos > sceneWidth - 25 && xPos < sceneWidth - 5 && yPos > p2BarBottomBound && yPos < p2BarBottomBound + 10 && sinDirection<0 && !rightSideHit) {      //Right bar top bound Hit
            System.out.printf("%f%n", sinDirection);
            rightSideHit = true;
            leftSideHit = true;
            ball.setDirection(Math.PI + direction + Math.random() * (Math.PI / 8) - (Math.PI / 16));
            ball.move(gui);
            bounceCount++;
        }
        lastRelXPos = xRelPos;
    }

    private void easySleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void reset() {
        bounceCount = 0;
        lastRelXPos = 0;
        leftSideHit = false;
        rightSideHit = false;
        gameEndEventListenerList.clear();
    }

    public void stop() {
        isRunning = false;
    }

    public void enableBallMovement(){
        updateBallMovement = true;
    }

    public void disableBallMovement(){
        updateBallMovement = false;
    }

    public void addGameEndEventListener(GameEndEventListener gameEndEventListener){
        gameEndEventListenerList.add(gameEndEventListener);
    }

    private void fireGameEndEvent(GameEndEvent gameEndEvent){
        for (GameEndEventListener gameEndEventListener : gameEndEventListenerList) {
            gameEndEventListener.onGameEnd(gameEndEvent);
        }
    }
}
